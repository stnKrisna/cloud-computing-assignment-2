<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  if (Auth::guest())
    return view('auth.login');
  else
    return redirect('home');
});

Route::any('/thirdPartyAuth', 'ThirdPartyAuthController@index')->name('thirdPartyAuth');
Route::get('/completeAccount', 'ProfileController@completeAccount')->name('completeAccountView');
Route::post('/completeAccount', 'ProfileController@completeAccountSave')->name('completeAccountSave');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::any('/search', 'SearchController@index')->name('search');

Route::get('/profile/{userHandle}', 'ProfileController@index')->name('profile');
Route::get('/profile/edit/{userHandle}', function () {
  return view('editProfile');
})->name('editProfile');
Route::post('/profile/edit/{userHandle}', 'ProfileController@save')->name('editProfileSave');
Route::post('/profile/{userHandle}', 'UserConnectionController@followAction')->name('followAction');

Route::post('userImageUpload', 'UserPostController@newPost')->name('newPost');

Route::get('/message', function () {
  return view('message');
})->name('message');

Route::get('/message/{partnerHandle}', function () {
  return view('message');
})->name('messagePartner');
Route::post('/message/{partnerHandle}', 'UserPostController@sendMessage')->name('sendNewMessage');

Route::get('/post/{postId}', 'UserPostController@readSinglePost')->name('viewPost');
Route::post('/post/{postId}', 'UserPostController@newComment')->name('postComment');

Route::post('/like/{postId}', 'UserPostController@like')->name('likePost');
Route::post('/dislike/{postId}', 'UserPostController@dislike')->name('dislikePost');
Route::post('/resetLike/{postId}', 'UserPostController@resetLike')->name('resetLikePost');

Route::post('/save-device-token', 'ProfileController@saveToken')->name('saveDeviceToken');

Route::get('/searchUser', 'SearchController@searchUserPaginate')->name('searchUser');
Route::get('/searchPost', 'SearchController@searchImagePaginate')->name('searchPost');
