/*
Give the service worker access to Firebase Messaging.
Note that you can only use Firebase Messaging here, other Firebase libraries are not available in the service worker.
*/
importScripts('https://www.gstatic.com/firebasejs/6.3.4/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/6.3.4/firebase-messaging.js');

/*
Initialize the Firebase app in the service worker by passing in the messagingSenderId.
* New configuration for app@pulseservice.com
*/
firebase.initializeApp({
  apiKey: "AIzaSyCNTwa1WrkhCy57S2FIJKAUSm_2O2wGy8g",
  authDomain: "cloudcomputingfinalproje-cd4a8.firebaseapp.com",
  databaseURL: "https://cloudcomputingfinalproje-cd4a8.firebaseio.com",
  projectId: "cloudcomputingfinalproje-cd4a8",
  storageBucket: "cloudcomputingfinalproje-cd4a8.appspot.com",
  messagingSenderId: "833691791828",
  appId: "1:833691791828:web:b01c7efa3366b17a36e641",
  measurementId: "G-44TM066327"
});

/*
Retrieve an instance of Firebase Messaging so that it can handle background messages.
*/
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body,
    icon: payload.notification.icon,
  };

  return self.registration.showNotification(notificationTitle,
      notificationOptions);
});
