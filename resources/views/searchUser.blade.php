@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Search result</div>

                <div class="card-body">
                    @if(isset($message))
                      <p> {{ $message }}</p>
                    @endif

                    <h2 style="display: block;width: 100%;">Users</h2>
                    @if(isset($users))

                      <div class="card-columns" style="width:100%;">
                        @foreach($users as $user)
                        <a href="{{ route('profile', $user->handle) }}" class="card userSearchResult">
                          <img class="card-img-top" src="{{ App\Http\Controllers\UserFileUploadController::fetch($user->profilePic) }}" alt="Card image cap">
                          <div class="card-body">
                            <h5 class="card-title" style="color:000;">{{ $user->name }}</h5>
                            <p style="color:#999;">{{ $user->handle }}</p>
                          </div>
                        </a>
                        @endforeach
                      </div>

                      @if (sizeof($users) >= 1)
                      {{ $users->links() }}
                      @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
