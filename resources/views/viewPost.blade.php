@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
              <div class="card-header">

              </div>

              <div class="card-body">
                  @if (session('status'))
                      <div class="alert alert-success" role="alert">
                          {{ session('status') }}
                      </div>
                  @endif

                  @include('partials.userPost', ['thePosts' => $thePosts, 'noReadMore' => true])
                  <h3>Comments</h3>
                  @if ($theComments->count() != 0)
                    @include('partials.userPost', ['thePosts' => $theComments, 'noReadMore' => true])
                  @else
                  <p>No comment for this post</p>
                  @endif

                  {{ Form::open(['route' => ['postComment', app('request')->route()->parameters['postId']], 'files' => true]) }}

                  <div>
                    <label><textarea class="form-control" rows="6" cols="100" name="userPost"></textarea></label>
                  </div>

                  <button type="submit" class="btn btn-primary">Post Comment</button>

                  {{ Form::close() }}
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
