@php
use App\Http\Controllers\ProfileController;
$partners = ProfileController::getMessagePartner();
@endphp

<div class="list-group list-group-flush" style="margin-bottom:20px;border-bottom:solid 1px #eee;">
  <p class="list-group-item"><b>Message Partners</b></p>
  @foreach ($partners as $partner)
   @if (array_key_exists('partnerHandle', app('request')->route()->parameters) && $partner['handle'] == app('request')->route()->parameters['partnerHandle'])
    <a class="list-group-item active" href="{{ route('messagePartner', $partner['handle']) }}">
   @else
    <a class="list-group-item" href="{{ route('messagePartner', $partner['handle']) }}">
   @endif
    {{ $partner['name'] }}</a>
  @endforeach
</div>
