@php
use App\Http\Controllers\UserPostController;
if (array_key_exists('partnerHandle', app('request')->route()->parameters)) {
  $messages = UserPostController::getMessages(app('request')->route()->parameters['partnerHandle']);
  $myId = Auth::user()->id;
  $skip = false;
} else {
  $skip = true;
}
@endphp

@if (!$skip)
<h4>Conversation</h4>
<ul class="list-group list-group-flush">
  @forelse ($messages as $message)
    @if ($message->from_id != $myId)
      <li class="list-group-item">
    @else
      <li class="list-group-item list-group-item-primary" style="text-align: right;">
    @endif

    <span>{{ $message->message }}</span> <br> <span style="color:grey; font-size:8pt;">{{ (new DateTime($message->created_at, new DateTimeZone('UTC')))
    ->setTimezone(new DateTimeZone('Australia/Melbourne'))
    ->format('M d, Y @ H:i a') }}</span></li>
  @empty
    <p>No message, yet. Type your message end click send to get started.</p>
  @endforelse
</ul>
@endif
