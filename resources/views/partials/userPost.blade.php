@if (isset($thePosts))
<div class="row">
  @forelse ($thePosts as $post)
  @if (isset($colSize))
    <div class="{{$colSize}}">
  @else
    <div class="col-sm-12">
  @endif
    <div class="card" style="margin: 25px 0;">
      <div class="card-header">
        <b><a href="profile/{{ $post->name }}">{{ $post->name }}</a></b> posted:
      </div>
      <div class="card-body">
        @if (isset($post->attachment_name))
        <img src="{{ App\Http\Controllers\UserFileUploadController::fetch($post->attachment_name) }}" style="max-width: 100%;" />
        @endif
        <p class="card-text">{{ $post->user_post }}</p>
      </div>
      <div class="card-footer">
        {{ (new DateTime($post->created_at, new DateTimeZone('UTC')))
        ->setTimezone(new DateTimeZone('Australia/Melbourne'))
        ->format('M d, Y @ H:i a') }}

        @if (array_key_exists('postId', app('request')->route()->parameters))

          @php
          if (App\Http\Controllers\UserPostController::myVote($post->id)->count() == 1)
            $myVote = App\Http\Controllers\UserPostController::myVote($post->id)[0]->votes;
          else
            $myVote = 0;
          @endphp

          @if ($myVote == 0 || $myVote == -1)
          | {{ Form::open(['route' => ['likePost', $post->id], 'style' => 'display:inline;']) }}
          <button type="submit" class="btn btn-primary">Like</button>
          {{ Form::close() }}
          @elseif ($myVote == 1)
          | {{ Form::open(['route' => ['resetLikePost', $post->id], 'style' => 'display:inline;']) }}
          <button type="submit" class="btn btn-light">Unvote</button>
          @endif

          <b>&nbsp;{{ App\Http\Controllers\UserPostController::getPostRating( $post->id ) }}&nbsp;</b>

          @if ($myVote == 0 || $myVote == 1)
          {{ Form::open(['route' => ['dislikePost', $post->id], 'style' => 'display:inline;']) }}
          <button type="submit" class="btn btn-danger">Dislike</button>
          {{ Form::close() }}
          @elseif ($myVote == -1)
          {{ Form::open(['route' => ['resetLikePost', $post->id], 'style' => 'display:inline;']) }}
          <button type="submit" class="btn btn-light">Unvote</button>
          {{ Form::close() }}
          @endif

        @else

        | <b>&nbsp;{{ App\Http\Controllers\UserPostController::getPostRating( $post->id ) }}&nbsp;</b>

        @endif

        @if(!isset($noReadMore))
        | <a href="{{ route('viewPost', $post->id) }}">Read More</a>
        @endif
      </div>
    </div>
  </div>
  @empty
    <p>No post found</p>
  @endforelse

  <div class="col-md-12">
    @if (!isset($showLinks))
    {{$thePosts->links()}}
    @endif
  </dv>
</div>
</div>
@endif
