@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Messages</div>

                <div class="card-body">
                    <h2>Messages</h2>

                    <div class="row">
                      <div class="col-sm-12 col-md-4">
                        @include('partials.messagePartner')
                      </div>
                      <div class="col-sm-12 col-md-8">
                        @include('partials.messages')

                        @if (array_key_exists('partnerHandle', app('request')->route()->parameters))
                        <form method="POST" action="{{route('sendNewMessage', app('request')->route()->parameters['partnerHandle'])}}">
                          @csrf
                          <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Your message" aria-label="Your message" aria-describedby="button-addon2" name="message">
                            <div class="input-group-append">
                              <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Send</button>
                            </div>
                          </div>
                        </form>
                        @endif
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
