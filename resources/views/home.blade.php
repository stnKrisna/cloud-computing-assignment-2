@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                  <h3>New Post</h3>
                  @if (session('errors'))
                      <div class="alert alert-success" role="alert">
                        {{ session('errors')->first() }}
                      </div>
                  @endif
                  @if (session('message'))
                      <div class="alert alert-success" role="alert">
                          {{ session('message') }}
                      </div>
                  @endif
                  <form action="{{ route('newPost') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">

                      <div class="col-md-12">
                        <textarea class="form-control" name="userPost" id="exampleFormControlTextarea1" rows="3" placeholder="Your message"></textarea>
                        <br />
                      </div>

                      <div class="col-md-6">
                        <p>Image attachment</p>
                        <input type="file" name="image" class="form-control">
                        <br />
                      </div>
                      <div class="col-md-6"></div>

                      <div class="col-md-6">
                        <button type="submit" class="btn btn-success">Post</button>
                      </div>

                    </div>
                  </form>
                </div>

                <div class="card-body">
                    @include('partials.userPost', ['thePosts' => $thePosts])
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
