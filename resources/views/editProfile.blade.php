@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h2>Edit Profile</h2>
                    @if (isset($resp))
                        <div class="alert alert-success" role="alert">
                            {{ $resp }}
                        </div>
                    @endif
                    {{ Form::open(['route' => ['editProfile', app('request')->route()->parameters['userHandle']], 'files' => true]) }}

                    <div>
                      <label>Your name: {{ Form::input('text', 'name', Auth::user()->name, null) }}</label>
                    </div>

                    <div>
                      <label>New Password: {{ Form::input('password', 'password', null, null) }}</label>
                    </div>

                    <div>
                      <label>Confirm New Password: {{ Form::input('password', 'password_confirm', null, null) }}</label>
                    </div>

                    <div>
                      <label>Current Profile Picture:<br /><img src="{{ App\Http\Controllers\UserFileUploadController::fetch(Auth::user()->profilePic) }}" width="100px" height="100px" /></label>
                    </div>

                    <div>
                      <label>New profile picture: <input type="file" name="image" class="form-control"></label>
                    </div>

                    <button type="submit" class="btn btn-primary">Save</button>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
