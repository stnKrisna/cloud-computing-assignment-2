@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (isset($status))
                <div class="alert alert-{{ $status['status'] }}" role="alert">
                    {!! $status['message'] !!}
                </div>
            @endif
            <div class="card">

                <div class="card-body">
                    <div class="card-group">
                      <div class="card" style="width: 100%; max-width: 100px; border-color: transparent; background:url('{{ App\Http\Controllers\UserFileUploadController::fetch(Auth::user()->profilePic) }}'); background-size:contain; background-repeat:no-repeat; background-position:center center;"></div>

                      <div class="card" style="border-color: transparent;">
                        <div class="card-body">
                          <h1>{{ $theUser->name }}</h1>

                          @if (Auth::check() && Auth::user()->id == $theUser->id)
                            <a href="{{route('editProfile', ['userHandle' => $theUser->handle])}}"><button class="btn btn-outline-primary">Edit Profile</button></a>
                          @else
                            @if ($followed)
                            <form action="{{ route('followAction', $theUser->handle) }}" method="post">
                              @csrf
                              <input type="hidden" value="unfollow" name="action" />
                              <button type="submit" class="btn btn-outline-danger">Unfollow</button>
                            </form>
                            @else
                            <form action="{{ route('followAction', $theUser->handle) }}" method="post">
                              @csrf
                              <input type="hidden" value="follow" name="action" />
                              <button type="submit" class="btn btn-outline-primary">Follow</button>
                            </form>
                            @endif

                            <a href="{{route('messagePartner', $theUser->handle)}}"><button class="btn btn-outline-primary">Direct Message</button></a>
                          @endif
                          <br>
                        </div>
                      </div>
                    </div>

                    <div style="width:100%;">
                      @include('partials.userPost', ['thePosts' => $myPosts])
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
