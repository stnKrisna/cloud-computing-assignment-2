@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Search result</div>

                <div class="card-body">
                    @if(isset($message))
                      <p> {{ $message }}</p>
                    @endif

                    <h2>Posts</h2>
                    @if(isset($photos))
                      @include('partials.userPost', ['thePosts' => $photos, 'colSize' => 'col-md-6 col-sm-12', 'showLinks' => false])
                      {{ $photos->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
