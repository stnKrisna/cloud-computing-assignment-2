<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Post extends Model
{
  protected $fillable = [
    'user_id', 'user_post', 'attachment_name', 'suggestive', 'parent_id',
  ];
}
