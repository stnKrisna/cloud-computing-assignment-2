<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Message extends Model
{
  protected $fillable = [
    'from_id', 'to_id', 'message',
  ];
}
