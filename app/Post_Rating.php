<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post_Rating extends Model
{
  protected $fillable = [
    'user_id', 'post_id', 'votes',
  ];
}
