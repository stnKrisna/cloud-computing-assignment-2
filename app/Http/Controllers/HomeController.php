<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $userFollowingPosts = DB::table('user__posts')
        ->select('user__posts.*','users.name')
        ->join('users', 'users.id', '=', 'user__posts.user_id')
        ->whereIn('user__posts.user_id', function($query){
          $query->select('user_b')
          ->from('user__connections')
          ->where('user_a', '=', Auth::id())
          ->get();
        })
        ->orWhere('user__posts.user_id', '=', Auth::id())
        ->orderByDesc('user__posts.created_at')
        ->where('user__posts.parent_id', '=', null)
        ->paginate(10);
        $request->session()->forget('status');
        return view('home')->withThePosts($userFollowingPosts);
    }
}
