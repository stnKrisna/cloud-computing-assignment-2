<?php

namespace App\Http\Controllers;

use Storage;
use Illuminate\Http\Request;
use Aws\Rekognition\RekognitionClient;

class UserFileUploadController extends Controller
{
  static public function rekognize (Request $request) {
    /*
    [2]“Amazon Rekognition 2016-06-27", Amazon, 2020. [Online]. Available: https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-rekognition-2016-06-27.html#detecttext. [Accessed: 7- May- 2020].
    */
    $client = new RekognitionClient([
      'region'    => 'us-east-1',
      'version'   => 'latest'
    ]);

    $image = fopen($request->file('image')->getPathName(), 'r');
    $bytes = fread($image, $request->file('image')->getSize());

    // Logic

    $nudity = false;
    $labels = [];

    // Process nudity filter
    $results = $client->detectModerationLabels(['Image' => ['Bytes' => $bytes], 'MinConfidence' => intval($request->input('confidence'))])['ModerationLabels'];

    if(array_search('Explicit Nudity', array_column($results, 'Name')))
    {
      $nudity = true;
    }

    // Process labels
    $results = $client->DetectLabels(['Image' => ['Bytes' => $bytes], 'MinConfidence' => intval($request->input('confidence'))])['Labels'];

    foreach($results as $item)
    {
      if ($item['Confidence'] >= 70) {
        array_push($labels, $item['Name']);
      }
    }

    return [
      'nudity' => $nudity,
      'labels' => $labels
    ];
  }

  static public function store(Request $request, $skipRekognize=false) {
    /*
    [4]“thephpleague/flysystem-aws-s3-v3”, GitHub, 2020. [Online]. Available: https://github.com/thephpleague/flysystem-aws-s3-v3. [Accessed: 7- May- 2020].
    [5]Laravel.com. 2020. File Storage - Laravel - The PHP Framework For Web Artisans. [online] Available at: <https://laravel.com/docs/6.x/filesystem> [Accessed 7 May 2020].
    */
    $useS3 = true;
    // $request->validate(['image' => 'required|image']);
    // return "asdasd";
    if($request->hasfile('image')) {
      $file = $request->file('image');
      $name=time().$file->getClientOriginalName();
      $filePath = 'images/' . $name;

      if ($useS3)
        Storage::disk('s3')->put($filePath, file_get_contents($file));
      else
        $file->move('images/', $name);

      return [
        'filename' => $name,
        'meta' => ($skipRekognize? null : UserFileUploadController::rekognize($request))
      ];
    } else {
      return null;
    }
  }

  public function newImagePost(Request $request) {
    if(UserFileUploadController::store($request))
      return view('home')->withStatus('success')->withMessage('Image uploaded successfully.');
  }

  static public function fetch($fileName) {
    /*
    [4]“thephpleague/flysystem-aws-s3-v3”, GitHub, 2020. [Online]. Available: https://github.com/thephpleague/flysystem-aws-s3-v3. [Accessed: 7- May- 2020].
    [5]Laravel.com. 2020. File Storage - Laravel - The PHP Framework For Web Artisans. [online] Available at: <https://laravel.com/docs/6.x/filesystem> [Accessed 7 May 2020].
    */
    return Storage::disk('s3')->url('images/'.$fileName);
  }
}
