<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use App\User_Post;
use App\Post_Label;
use App\Post_Rating;
use App\User_Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\UserFileUploadController;

class UserPostController extends Controller
{
  public function __construct()
    {
      $this->serverKey = config('app.firebase_server_key');
    }

  public function newPost(Request $request) {
    $validator = Validator::make($request->all(),[
        'image' => 'required|mimes:jpg,jpeg,png |max:2048',
    ],$messages = [
        'mimes' => 'Please insert JPG or PNG only',
        'max'   => 'Image should be less than 2 MB'
    ]);

    $validator->after(function ($validator) {});

    if ($validator->fails()) {
      $errors = $validator->errors();
      Session::flash('errors', $errors);
      return redirect('home');
    }

    $attachment = UserFileUploadController::store($request);
    $attachmentName = null;
    $attachmentMeta = [
      'nudity' => false,
      'labels' => []
    ];
    $userPost = $request->input( 'userPost' );

    if (isset($attachment)) {
      $attachmentName = $attachment['filename'];
      $attachmentMeta = $attachment['meta'];
    }

    if (!isset($userPost)) {
      $userPost = '';
    }

    $newPostId = User_Post::create([
        'user_id' => Auth::id(),
        'user_post' => $userPost,
        'attachment_name' => $attachmentName,
        'suggestive' => $attachmentMeta['nudity']
    ])->id;

    // Save labels
    foreach($attachmentMeta['labels'] as $label)
    {
      Post_Label::create([
        'post_id' => $newPostId,
        'label' => $label
      ]);
    }

    $userFollowingPosts = DB::table('user__posts')
    ->select('user__posts.*','users.name')
    ->join('users', 'users.id', '=', 'user__posts.user_id')
    ->whereIn('user__posts.user_id', function($query){
      $query->select('user_b')
      ->from('user__connections')
      ->where('user_a', '=', Auth::id())
      ->get();
    })
    ->orWhere('user__posts.user_id', '=', Auth::id())
    ->orderByDesc('user__posts.created_at')
    ->paginate(10);

    Session::flash('status', 'success');
    Session::flash('message', 'New post added');
    return redirect('home')->withStatus('success')->withMessage('New post added')->withThePosts($userFollowingPosts)->withAttachmentMeta($attachmentMeta);
  }

  static public function getPostByUser ($userHandle) {
    $posts = DB::table('user__posts')
              ->join('users', 'users.id', '=', 'user__posts.user_id')
              ->where('handle', '=', $userHandle)
              ->where('user__posts.parent_id', '=', null)
              ->orderByDesc('user__posts.created_at')
              ->paginate(10);

    return $posts;
  }

  static public function search ($terms) {
    return DB::table('user__posts')
    ->select('user__posts.*', 'users.name', 'users.handle')
    ->join('post_labels', 'post_labels.post_id', '=', 'user__posts.id')
    ->join('users', 'user__posts.user_id', '=', 'users.id')
    ->whereIn('post_labels.label', $terms)
    ->distinct()
    ->orderByDesc('user__posts.created_at')
    ->paginate(10);
  }

  static public function getMessages ($partnerHandle) {
    $partnerId = DB::table('users')
    ->select('users.id')
    ->where('users.handle', '=', $partnerHandle)
    ->get();

    if ($partnerId->count() != 1) {
      return null;
    }

    $myId = Auth::user()->id;
    $messages = DB::table('user__messages')
    ->where(function($query) use ($myId, $partnerId) {
      $query->where('from_id', '=', $myId)
      ->Where('to_id', '=', $partnerId[0]->id);
    })
    ->orWhere(function($query) use ($myId, $partnerId) {
      $query->where('to_id', '=', $myId)
      ->Where('from_id', '=', $partnerId[0]->id);
    })
    ->get();

    return $messages;
  }

  public function sendMessage (Request $request, $partnerHandle) {
    $partnerId = DB::table('users')
    ->select('users.id', 'users.device_token')
    ->where('users.handle', '=', $partnerHandle)
    ->get();

    if ($partnerId->count() != 1) {
      return null;
    }

    User_Message::create([
      'from_id' => Auth::user()->id,
      'to_id' => $partnerId[0]->id,
      'message' => $request->get('message')
    ]);

    // Trigger notification
    $myHandle = Auth::user()->handle;
    $data = [
            "to" => $partnerId[0]->device_token,
            "notification" =>
                [
                    "title" => 'New message from ' . $myHandle,
                    "body" => $request->get('message'),
                    "url" => route('messagePartner', $myHandle)
                ],
    ];
    $dataString = json_encode($data);

    $headers = [
        'Authorization: key=' . $this->serverKey,
        'Content-Type: application/json',
    ];

    $ch = curl_init();

    /*
    [7]"Get Started on Web  |  Firebase", Firebase, 2020. [Online]. Available: https://firebase.google.com/docs/storage/web/start. [Accessed: 8- May- 2020].
    [8]"Build app server send requests  |  Firebase", Firebase, 2020. [Online]. Available: https://firebase.google.com/docs/cloud-messaging/send-message. [Accessed: 9- May- 2020].
    [9]Php.net. 2020. PHP: Curl - Manual. [online] Available at: <https://www.php.net/manual/en/book.curl.php> [Accessed 9 May 2020].
    */

    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

    if( ! $result = curl_exec($ch))
    {
        trigger_error(curl_error($ch));
    }
    curl_getinfo($ch);
    curl_close($ch);

    return redirect()->route('messagePartner', $partnerHandle);
  }

  public function readSinglePost (Request $request, $postId) {
    $thePosts = DB::table('user__posts')
    ->select('user__posts.*', 'users.name', 'users.profilePic', 'users.handle')
    ->join('users', 'users.id', '=', 'user__posts.user_id')
    ->where('user__posts.id', '=', $postId)
    ->paginate(10);

    $theComments = DB::table('user__posts')
    ->select('user__posts.*', 'users.name', 'users.profilePic', 'users.handle')
    ->join('users', 'users.id', '=', 'user__posts.user_id')
    ->where('parent_id', '=', $postId)
    ->paginate(10);

    return view('viewPost')->withThePosts($thePosts)->withTheComments($theComments);
  }

  public function newComment (Request $request, $postId) {
    User_Post::create([
        'user_id' => Auth::id(),
        'user_post' => $request->input( 'userPost' ),
        'attachment_name' => null,
        'suggestive' => false,
        'parent_id' => $postId
    ]);

    return $this->readSinglePost($request, $postId);
  }

  static public function myVote ($postId) {
    return DB::table('post__ratings')
    ->where('user_id', '=', Auth::user()->id)
    ->where('post_id', '=', $postId)
    ->get();
  }

  public function like (Request $request, $postId) {
    $rating = Post_Rating::updateOrCreate(
      ['user_id' => Auth::user()->id, 'post_id' => $postId],
      ['votes' => 1
    ]);
    $rating->save();
    return back()->withInput();
  }

  public function dislike (Request $request, $postId) {
    $rating = Post_Rating::updateOrCreate(
      ['user_id' => Auth::user()->id, 'post_id' => $postId],
      ['votes' => -1
    ]);
    $rating->save();
    return back()->withInput();
  }

  public function resetLike (Request $request, $postId) {
    $rating = Post_Rating::updateOrCreate(
      ['user_id' => Auth::user()->id, 'post_id' => $postId],
      ['votes' => 0
    ]);
    $rating->save();
    return back()->withInput();
  }

  static public function getPostRating ($postId) {
    return DB::table('post__ratings')
    ->where('post__ratings.post_id', '=', $postId)
    ->sum('votes');
  }
}
