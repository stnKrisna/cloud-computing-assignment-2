<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\UserPostController;

class SearchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function searchUser ($query) {
      $users = DB::table('users')
                ->where('name', 'like', '%'. $query .'%')
                ->get();

      return $users;
    }

    public function index (Request $request) {
      $q = $request->input( 'q' );
      $users = $this->searchUser($q);
      $resultCount = count($users);
      $photoResult = UserPostController::search(explode(' ', $q));

      return view('search')->withUsers($users)->withQuery( $q )->withResultCount($resultCount)->withPhotos($photoResult);
    }

    public function searchUserPaginate(Request $request) {
      $q = $request->input( 'q' );
      $users = DB::table('users')
                ->where('name', 'like', '%'. $q .'%')
                ->paginate(10);
      $resultCount = count($users);

      return view('searchUser')->withUsers($users->appends($_GET))->withQuery( $q )->withResultCount($resultCount);
    }

    public function searchImagePaginate(Request $request) {
      $q = $request->input( 'q' );
      $photoResult = UserPostController::search(explode(' ', $q));

      return view('searchPost')->withQuery( $q )->withPhotos($photoResult->appends($_GET));
    }
}
