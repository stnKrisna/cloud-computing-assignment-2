<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\UserPostController;
use App\Http\Controllers\UserConnectionController;
use App\Http\Controllers\UserFileUploadController;
use Aws\CognitoIdentityProvider\CognitoIdentityProviderClient;

class ProfileController extends Controller
{
  public function getTheUser ($userHandle) {
    $users = DB::table('users')
              ->where('handle', '=', $userHandle)
              ->get();

    return $users;
  }

  public function index (Request $request) {
    $theUser = $this->getTheUser($request->userHandle);
    $isFollowed = UserConnectionController::userHasFollow(Auth::id(), $theUser[0]->id);

    if (count($theUser) == 1) {
      return view('profile')->withTheUser($theUser[0])->withFollowed($isFollowed)->withMyPosts(UserPostController::getPostByUser($request->userHandle));
    } else {
      return abort(404);
    }
  }

  public function save (Request $request) {
    $user = Auth::user();
    $resp = '';

    if (!$user) {
      return view('home');
    }

    if (!empty(request('name')) && request('name') != $user->name) {
      $user->name = request('name');
      $resp .= 'Username has been changed. ';
    }

    if (!empty(request('password')) && request('password') == request('password_confirm')) {
      // Change amazon cognito password
      $config = [
        'credentials' => config('cognito.credentials'),
        'region'      => config('cognito.region'),
        'version'     => config('cognito.version')
      ];
      $client = new CognitoIdentityProviderClient($config);
      $client->AdminSetUserPassword([
          'Username' => Auth::user()->email,
          'UserPoolId' => config('cognito.user_pool_id'),
          'Permanent' => true,
          'Password' => request('password')
      ]);

      $resp .= 'Password has been changed. ';
    }

    $profilePic = UserFileUploadController::store($request, true);
    if ($profilePic != null) {
      $user->profilePic = $profilePic['filename'];
      $resp .= 'Profile picture has been changed. ';
    }

    $user->save();

    if ($resp == '') {
      $resp = null;
    }

    return view('editProfile', ['userHandle' => $user->handle])->withResp($resp);
  }

  static public function getMessagePartner () {
    $messagePartners = DB::table('user__messages')
    ->select('user_from.name as fromName', 'user_from.handle as fromHandle', 'user_to.name as toName', 'user_to.handle as toHandle')
    ->join('users as user_from', 'user_from.id', '=', 'user__messages.from_id')
    ->join('users as user_to', 'user_to.id', '=', 'user__messages.to_id')
    ->where('user__messages.from_id', '=', Auth::user()->id)
    ->orWhere('user__messages.to_id', '=', Auth::user()->id)
    ->distinct()
    ->get();

    if ($messagePartners->count() == 0) {
      return [];
    }

    $ret = [];
    $userHandle = Auth::user()->handle;
    foreach ($messagePartners as $messagePartner) {
      if ($messagePartner->fromHandle != $userHandle) {
        array_push($ret, [
          'handle' => $messagePartner->fromHandle,
          'name' => $messagePartner->fromName
        ]);
      }
      if ($messagePartner->toHandle != $userHandle) {
        $isInList = false;
        foreach ($ret as $partnerInList) {
          if ($partnerInList['handle'] == $messagePartner->toHandle) {
            $isInList = true;
            break;
          }
        }
        if (!$isInList) {
          array_push($ret, [
            'handle' => $messagePartner->toHandle,
            'name' => $messagePartner->toName
          ]);
        }
      }
    }

    return $ret;

  }

  public function saveToken (Request $request)
  {
      $user = User::find($request->user_id);
      $user->device_token = $request->fcm_token;
      $user->save();

      if($user)
          return response()->json([
              'message' => 'User token updated'
          ]);

      return response()->json([
          'message' => 'Error!'
      ]);
  }

  public function completeAccount (Request $request) {
    return view('completeAccount');
  }
}
