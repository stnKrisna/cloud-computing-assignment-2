<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\User_Connection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\UserPostController;

class UserConnectionController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public static function userHasFollow ($userA, $userB) {
    $ret = DB::table('user__connections')
              ->where('user_a', '=', $userA)
              ->where('user_b', '=', $userB)
              ->get();
    return count($ret) != 0;
  }

  public function followAction (Request $request) {
    switch ($request->action) {
      case 'follow': return $this->follow($request);
      case 'unfollow': return $this->unfollow($request);

      default: return abort(404);
    }
  }

  public function follow(Request $request)
  {
    $users = DB::table('users')
              ->where('handle', '=', $request->userHandle)
              ->get();
    $user_b_id = $users[0]->id;

    $isFollowed = false;

    if (count($users) == 1) {
      // Check if user a is already following user b
      $hasFollow = $this->userHasFollow(Auth::id(), $user_b_id);

      if (!$hasFollow) {
        User_Connection::create([
            'user_a' => Auth::id(),
            'user_b' => $user_b_id,
            'confirmed' => true,
        ]);

        $request->session()->put('status', 'success');
        $isFollowed = true;
      } else {
        $request->session()->put('status', 'success');
        $isFollowed = true;
      }
    } else {
      $request->session()->put('status', 'error');
    }

    return view('profile')->withTheUser($users[0])->withFollowed($isFollowed)->withStatus([
      'status' => 'success',
      'message' => 'You followed <b>' . $users[0]->name .'</b>'
    ])->withMyPosts(UserPostController::getPostByUser($request->userHandle));
  }

  public function unfollow(Request $request)
  {
    $users = DB::table('users')
              ->where('handle', '=', $request->userHandle)
              ->get();
    $user_b_id = $users[0]->id;

    User_Connection::where('user_a', '=', Auth::id())
    ->where('user_b', '=', $user_b_id)
    ->delete();

    return view('profile')->withTheUser($users[0])->withFollowed(false)->withStatus([
      'status' => 'success',
      'message' => 'You unfollowed <b>' . $users[0]->name . '</b>'
    ])->withMyPosts(UserPostController::getPostByUser($request->userHandle));
  }
}
