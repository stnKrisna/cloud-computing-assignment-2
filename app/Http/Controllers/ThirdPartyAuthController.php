<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\User;
use Illuminate\Http\Request;
use Psr\Http\Message\ResponseInterface;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Provider\AbstractProvider;

class CustomProvider extends AbstractProvider {

  static public function getLoginUrl($domain, $region, $provider, array $options = [])
  {
      $baseUrl = 'https://'.$domain.'.auth.'.$region.'.amazoncognito.com';
      $base   = $baseUrl . '/login';
      $params = $provider->getAuthorizationParameters($options);
      $query  = $provider->getAuthorizationQuery($params);

      $query = trim($query, '?&');

      if ($query) {
          $glue = strstr($base, '?') === false ? '?' : '&';
          return $base . $glue . $query;
      }

      return $base;
  }

  public function getBaseAuthorizationUrl(){}
  public function getBaseAccessTokenUrl(array $params){}
  public function getResourceOwnerDetailsUrl(AccessToken $token){}
  protected function getDefaultScopes(){}
  protected function checkResponse(ResponseInterface $response, $data){}
  protected function createResourceOwner(array $response, AccessToken $token){}
}

class ThirdPartyAuthController extends Controller
{

  /*
  [14]"Identity Pools (Federated Identities) Authentication Flow - Amazon Cognito", Docs.aws.amazon.com, 2020. [Online]. Available: https://docs.aws.amazon.com/cognito/latest/developerguide/authentication-flow.html. [Accessed: 21- May- 2020].
  [18]"Understanding Amazon Cognito user pool OAuth 2.0 grants | Amazon Web Services", Amazon Web Services, 2020. [Online]. Available: https://aws.amazon.com/blogs/mobile/understanding-amazon-cognito-user-pool-oauth-2-0-grants/. [Accessed: 22- May- 2020].
  */

  public function index (Request $request) {
    // Skip authentication if user is logged in
    if (Auth::check()) {
      return redirect(route('home'));
    }

    $providerConfig = [
      'clientId'          => config('cognito.app_client_id'),
      'clientSecret'      => config('cognito.app_client_secret'),
      'redirectUri'       => route('thirdPartyAuth'),
      // 'redirectUri'       => 'http://localhost:8000/thirdPartyAuth',
      'cognitoDomain'     => config('cognito.client_domain'),
      'region'            => config('cognito.region')
    ];

    $provider = new \CakeDC\OAuth2\Client\Provider\Cognito($providerConfig);

    if (!isset($_GET['code'])) {
      // Get login URL
      $loginUrl = CustomProvider::getLoginUrl($providerConfig['cognitoDomain'], $providerConfig['region'], $provider);

      // Set csrf token, dont forget to save the session
      Session::put('csrfLoginState', $provider->getState());
      Session::save();

      // Redirect user to the login portal
      return redirect($loginUrl);

  } elseif (empty($_GET['state']) || ($_GET['state'] !== Session::pull('csrfLoginState'))) { // Validate CSRF state

      // Redirect back to homepage if the user has an invalid CSRF token
      return redirect(route('home'));

    } else {

      // Try to get an access token (using the authorization code grant)
      $token = $provider->getAccessToken('authorization_code', [
          'code' => $_GET['code']
      ]);

      // Optional: Now you have a token you can look up a users profile data
      try {

          // We got an access token. Check if user exist in db, then authenticate them
          $user = $provider->getResourceOwner($token);
          $userEmail = $user->getEmail();

          // Use these details to create a new profile
          $dbUser = User::where('email', $userEmail);

          if ($dbUser->count() == 1) {
            Auth::login($dbUser->first());

            // Send user to home
            return redirect(route('home'));
          } else {
            // No user found in our DB. Let them create a new account
            session(['_old_input.email' => $userEmail]);
            return redirect(route('completeAccountView'))->withMessage('Please comlete your profile to continue');
          }

      } catch (Exception $e) {
          // User does not exist. Create new user with default data
          // Redirect user to register
          return redirect(route('register'));
      }
    }
  }
}
