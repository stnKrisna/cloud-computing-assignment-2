<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post_label extends Model
{
  protected $fillable = [
    'post_id', 'label',
  ];
}
