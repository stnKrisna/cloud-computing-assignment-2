<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Connection extends Model
{
  protected $fillable = [
    'user_a', 'user_b', 'confirmed', 'handle',
  ];
}
