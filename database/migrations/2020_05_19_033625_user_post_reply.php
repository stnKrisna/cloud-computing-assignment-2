<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserPostReply extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('user__posts', function (Blueprint $table) {
        $table->unsignedBigInteger('parent_id')->nullable();

        $table->foreign('parent_id')->references('id')->on('user__posts')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('user__posts', function (Blueprint $table) {
        $table->dropColumn('parent_id');
      });
    }
}
