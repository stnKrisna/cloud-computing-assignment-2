<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user__connections', function (Blueprint $table) {
            $table->unsignedBigInteger('user_a');
            $table->unsignedBigInteger('user_b');
            $table->boolean('confirmed');
            $table->timestamps();

            $table->foreign('user_a')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('user_b')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user__connections');
    }
}
