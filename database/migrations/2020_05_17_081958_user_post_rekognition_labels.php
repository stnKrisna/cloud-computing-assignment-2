<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserPostRekognitionLabels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('user__posts', function (Blueprint $table) {
        $table->boolean('suggestive')->default(0);
      });

      Schema::create('post_labels', function (Blueprint $table) {
          $table->unsignedBigInteger('post_id');
          $table->foreign('post_id')->references('id')->on('user__posts')->onDelete('cascade');
          $table->string('label');

          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('user__posts', function (Blueprint $table) {
        $table->dropColumn('suggestive');
      });

      Schema::dropIfExists('post_labels');
    }
}
